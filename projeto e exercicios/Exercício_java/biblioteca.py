from math import sqrt, floor
#import math
print('='*50)
num = int(input('Digite um número: '))
print('A raiz de {} é igual a {}'.format(num, floor(sqrt(num))))
print('='*50)
